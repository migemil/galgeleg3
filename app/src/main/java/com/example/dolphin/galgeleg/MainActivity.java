package com.example.dolphin.galgeleg;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button knapGalgeleg;
    Button knapIndstillinger;
    Button knapGetOrd;
    TextView ordFraDr;
    File fileLokation;
    String[] ordListeDR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        knapGalgeleg = (Button) findViewById(R.id.galgelegbutton);
        knapIndstillinger = (Button) findViewById(R.id.indstillingerbutton);
        knapGetOrd = (Button) findViewById(R.id.getdrord);
        ordFraDr = (TextView) findViewById(R.id.ordtilgalgeleg);

        //ordFraDr.setText(ordListeDR.toString());

        fileLokation = getFilesDir();

        knapGalgeleg.setOnClickListener(this);
        knapIndstillinger.setOnClickListener(this);
        knapGetOrd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == knapGalgeleg) {
            //Intent intent = new Intent(this, GalgelegGame.class);
            Intent intent = new Intent(this, ListOfWords.class);
            if (ordListeDR == null) {
                intent.putExtra("ordliste", new String[]{"bob", "char"});
            } else {
                intent.putExtra("ordliste", ordListeDR);
            }
            //intent.putExtra("ordet", "bog");
            startActivity(intent);
        }

        if (v == knapIndstillinger){
            knapIndstillinger.setText("Øv bøv, ingen funktion");
        }

        if (v == knapGetOrd){
            getOrdFraDr();
        }

    }

    public void getOrdFraDr(){
        knapGetOrd.setText("Henter ord");

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String titler = prefs.getString("titler", "Trold der ikke nogen"); // Henter fra prefs, og hvis der intet er sætter parameter 2 som default

        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... arg0) {
                try {
                    String rssdata = hentUrl("http://www.dr.dk/nyheder/service/feeds/allenyheder");
                    String titler = findTitler(rssdata);
                    prefs.edit().putString("titler", titler).commit();     // Gem i prefs
                    return titler;
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Øv Fejl";
                }
            }

            @Override
            protected void onPostExecute(Object titler) {
                //ordListeDR = liste;
                //ordListeDR = lavOrdListe((String)titler);
                //Log.d("Ordliste2312 ", lavOrdListe((String)titler).toString());
                //Log.d("Ordliste", prefs.getString("titler", "Der var ikke nogen"));
                ordListeDR = lavOrdListe((String)titler);
                Log.d("Element1", lavOrdListe((String)titler)[0]);
                Log.d("Element1", lavOrdListe((String)titler)[1]);
                Log.d("Element1", lavOrdListe((String)titler)[2]);
                ordFraDr.setText("Der er " + lavOrdListe((String)titler).length + " fra DR at vælge immellem.");
            }
        }.execute();
    }


    //Hjælpe metode som konvertere vores string til et String[] som vi kan sende videre


    //Hjælpe metoder lånt fra jakob er doven da den her ikke tæller med i karakter.
    private static String hentUrl(String url) throws IOException {
        System.out.println("Henter "+url);
        BufferedReader br = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
        StringBuilder sb = new StringBuilder();
        String linje = br.readLine();
        while (linje != null) {
            sb.append(linje + "\n");
            linje = br.readLine();
            System.out.println(linje);
        }
        br.close();
        return sb.toString();
    }

    private static String findTitler(String rssdata) {
        String titler = "";
        while (true) {
            int tit1 = rssdata.indexOf("<title>") + 7;
            int tit2 = rssdata.indexOf("</title>");
            if (tit2 == -1) break; // hop ud hvis der ikke er flere titler
            if (titler.length() > 400) break; // .. eller hvis vi har nok
            String titel = rssdata.substring(tit1, tit2);
            System.out.println(titel);
            titler = titler + titel + "\n";
            rssdata = rssdata.substring(tit2 + 8); // Søg videre i teksten efter næste titel
        }
        return titler;
    }

    private String[] lavOrdListe(String titler){

        titler = titler.replaceAll("\\]\\]\\>","");
        titler = titler.replaceAll("\\<\\!\\[CDATA\\[","");
        String[] ordListe = titler.split("\\s+");
        return ordListe;
    }
}
