package com.example.dolphin.galgeleg;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class GalgelegGame extends AppCompatActivity implements View.OnClickListener {

    AsyncTask async;
    Galgelogik spil;
    Button knap;
    EditText input;
    ImageView galge;
    TextView output;
    int forkerte;
    String ordet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galgeleg);

        spil = new Galgelogik();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null){
                ordet = null;
            } else{
                ordet = extras.getString("ordet");
            }
        } else {
            ordet = (String) savedInstanceState.getSerializable("ordet");
        }

        spil.nulstil(ordet);
        knap = (Button) findViewById(R.id.buttonGæt);
        input = (EditText) findViewById(R.id.userInput);
        galge = (ImageView) findViewById(R.id.imageView);
        output = (TextView) findViewById(R.id.textView);

        galge.setImageResource(R.drawable.galge);
        forkerte = spil.getAntalForkerteBogstaver();
        output.setText(spil.getSynligtOrd() + "  Forkerte: " + spil.getAntalForkerteBogstaver());

        knap.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        spil.gætBogstav(input.getText().toString());
        output.setText(spil.getSynligtOrd() + "  Forkerte: " + spil.getAntalForkerteBogstaver());

        if (spil.getAntalForkerteBogstaver() > forkerte) {
            switch (spil.getAntalForkerteBogstaver()) {
                case 1: galge.setImageResource(R.drawable.forkert1); break;
                case 2: galge.setImageResource(R.drawable.forkert2); break;
                case 3: galge.setImageResource(R.drawable.forkert3); break;
                case 4: galge.setImageResource(R.drawable.forkert4); break;
                case 5: galge.setImageResource(R.drawable.forkert5); break;
                case 6: galge.setImageResource(R.drawable.forkert6); break;
            }
        }

        if (spil.erSpilletSlut()) {
            if (spil.erSpilletVundet()) {
                output.setText("YAY DU VANDT ORDET VAR: " + spil.getOrdet());
            } else {
                output.setText("Øv du tabte ORDET VAR: " + spil.getOrdet());
            }
            input.setVisibility(View.INVISIBLE);
            knap.setVisibility(View.INVISIBLE);
            async = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    SystemClock.sleep(2000);
                    return null;
                }
                @Override
                protected void onPostExecute(Object result) {
                    //Null pointer exception rammer forkert gider ikke finde fejlen nu
                    finish();
                }
            }.execute();
        }
        input.setText("");
    }

    @Override
    protected void onDestroy() {
        if (async != null) async.cancel(true);
        super.onDestroy();
    }
}
