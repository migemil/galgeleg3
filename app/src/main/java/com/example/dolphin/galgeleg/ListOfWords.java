package com.example.dolphin.galgeleg;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListOfWords extends Activity implements AdapterView.OnItemClickListener {

    private String[] listeAfOrd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.d("detDer", getIntent().getExtras().getString("ordet"));
        //TODO ÆNDRER SÅ VI FÅR ET ARRAY IND MED ORD FRA DR
        //String[] listeAfOrdIni = {"Badedyr", "Trolde"};
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        listeAfOrd = lavOrdListe(prefs.getString("titler", "bjørn tudse").toLowerCase());

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.layout_wordlist, R.id.textView, listeAfOrd);

        ListView listView = new ListView(this);
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);

        setContentView(listView);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(this, GalgelegGame.class);
        intent.putExtra("ordet", listeAfOrd[position]);
        startActivity(intent);
        //Log.d("onItemClick", "Parent: "+parent+", /nView: "+view+", Position: "+position+", id: "+id);
    }

    private String[] lavOrdListe(String titler){

        titler = titler.replaceAll("\\]\\]\\>","");
        titler = titler.replaceAll("\\<\\!\\[CDATA\\[","");
        titler = titler.replaceAll("\\<\\!\\[cdata\\[","");
        titler = titler.replaceAll("\\.","");
        titler = titler.replaceAll("\\:","");
        String[] ordListe = titler.split("\\s+");
        return ordListe;
    }
}
